package com.wowtour.frame.utils;


import java.util.UUID;

public class UUIDUtil {

	public static String uuid(){
		return UUID.randomUUID().toString();
	}
}
